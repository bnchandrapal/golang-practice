// Golang program to print the commandline arguments
package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("01. Echo program")
	fmt.Println("Program name: " + os.Args[0])

	for index, name := range os.Args[1:] {
		fmt.Println("Index: " + strconv.Itoa(index) + ", Value: " + name)
	}

	if len(os.Args) > 1 {
		fmt.Println("The entire argument list: " + strings.Join(os.Args[1:], " "))
	} else {
		fmt.Println("You didnt pass any arguments")
	}
}
