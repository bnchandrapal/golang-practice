// Program to create maps and use them
package main

import (
	"fmt"
)

func main() {
	ages := make(map[string]int)
	ages["david"] = 18
	ages["billa"] = 20
	for name, age := range ages {
		fmt.Println("Age of", name, "is:", age)
	}

	name := map[string]string{
		"salil": "kumar",
		"sak":   "eth",
	}
	name["test"] = "test"

	fmt.Println("Names: ")
	for first, last := range name {
		fmt.Println(first, last)
	}
}
