// Golang example to show sorting
package main

import (
	"fmt"
	"sort"
)

func main() {
	str := []string{"desouza", "abraham", "linkedin"}

	sort.Strings(str) // sorts in ascending order

	for _, s := range str {
		fmt.Println(s)
	}
}
