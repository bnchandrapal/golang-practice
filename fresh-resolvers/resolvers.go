// Golang program to get a fresh list of working DNS resolvers
package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/Chan9390/dnsutil"
)

func main() {
	/*
		if _, err := os.Stat("/tmp/resolvers"); os.IsNotExist(err) {
			fmt.Println("[+] Creating temporary resolvers dir")
			os.Mkdir("/tmp/resolvers", 644)
		} else {
			fmt.Println("Exists")
		}*/
	resp, err := http.Get("http://public-dns.info/nameservers.txt")
	if err != nil {
		fmt.Fprintf(os.Stderr, "[-] Error when accessing the resolver list: %v\n", err)
		os.Exit(1)
	}
	b, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "[-] Error when reading the response: %v\n", err)
		os.Exit(1)
	}
	nameServerList := strings.Split(string(b), "\n")
	test := getDNSFromGoogle()
	for _, name := range nameServerList {
		tmp := checkDNS(name)
		if test == tmp {
			fmt.Println(name)
		}
	}
}

func getDNSFromGoogle() string {
	var dig dnsutil.Dig
	dig.SetDNS("8.8.8.8")
	a, err := dig.A("example.com")
	if err != nil {
		fmt.Fprintf(os.Stderr, "[-] Error when getting DNS reply from Google: %v\n", err)
		return ""
	}
	return a[0].A.String()
}

func checkDNS(serverAddr string) string {
	var dig dnsutil.Dig
	dig.SetDNS(serverAddr)
	dig.SetTimeOut(500 * time.Millisecond)
	a, err := dig.A("example.com")
	if err != nil {
		//fmt.Fprintf(os.Stderr, "[-] Error when getting DNS reply from server %s: %v\n", serverAddr, err)
		// generally you get i/o timeout error
		return ""
	}
	return a[0].A.String()
}
